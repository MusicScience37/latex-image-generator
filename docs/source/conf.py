# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

# Ignore some errors / warnings unimportant for conf.py.
# pylint: disable=invalid-name,redefined-builtin,missing-module-docstring
# type: ignore
# noqa D100

import pathlib
import sys

import toml

sys.path.append(str(pathlib.Path(__file__).absolute().parent.parent.parent))


def read_version() -> str:
    """Read version from pyproject.toml file.

    Returns:
        str: Version.
    """
    this_dir = pathlib.Path(__file__).absolute().parent
    root_dir = this_dir.parent.parent
    pyproject_toml_path = root_dir / "pyproject.toml"
    config = toml.load(str(pyproject_toml_path))
    version = str(config["tool"]["poetry"]["version"])
    return version


project = "latex-image-generator"
copyright = "2022, Kenta Kabashima"
author = "Kenta Kabashima"
release = read_version()

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["sphinx.ext.autodoc", "sphinx.ext.napoleon"]

templates_path = ["_templates"]
exclude_patterns = []

# Markdown
extensions += ["myst_parser"]


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_orange_book_theme"
html_static_path = ["_static"]

html_title = project

html_theme_options = {
    "show_prev_next": False,
    "pygment_light_style": "gruvbox-light",
    "pygment_dark_style": "native",
    "repository_url": "https://gitlab.com/MusicScience37Projects/tools/latex-image-generator",
    "use_repository_button": True,
}
