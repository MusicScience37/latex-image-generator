#!/bin/bash

set -e

cd $(dirname $0)

./update_apidoc.sh

sphinx-autobuild source build --port 5458
