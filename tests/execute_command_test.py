"""Test of execute_command.py."""

import asyncio
import pathlib
import sys
import typing

import pytest

from latex_image_generator.execute_command import execute_command

THIS_DIR = pathlib.Path(__file__).absolute().parent


class TestExecuteCommand:
    """Tests of execute_command function."""

    @pytest.mark.asyncio
    async def test_execute_command_successfully(self) -> None:
        """Execute a command successfully."""
        output = await execute_command(["latex", "--version"])
        assert "TeX" in output

    @pytest.mark.asyncio
    async def test_execute_nonexisting_command(self) -> None:
        """Try to execute a non-existing command."""
        with pytest.raises(Exception):
            await execute_command(["invalid-command"])

    @pytest.mark.asyncio
    async def test_execute_command_to_fail(self) -> None:
        """Execute a command to fail."""
        with pytest.raises(Exception):
            await execute_command(["platex"])

    @pytest.mark.asyncio
    async def test_cancel_execution(self) -> None:
        """Cancel execution."""
        task = asyncio.create_task(execute_command(["sleep", "10"]))
        await asyncio.sleep(0.1)
        task.cancel()
        with pytest.raises(asyncio.CancelledError):
            await task

    @pytest.mark.asyncio
    async def test_cancel_execution_to_fail_in_timeout(self) -> None:
        """Cancel execution."""
        python_executable: typing.Optional[str] = sys.executable
        if python_executable is None or python_executable == "":
            python_executable = "python"
        task = asyncio.create_task(
            execute_command(
                [python_executable, str(THIS_DIR / "trap_sigterm.py")],
                cancel_timeout=0.1,
            )
        )
        await asyncio.sleep(1.0)
        task.cancel()
        with pytest.raises(asyncio.CancelledError):
            await task
