"""Test of generate_image.py."""

import pathlib

import pytest

from latex_image_generator.generate_image import generate_image

from .remove_temp_files import remove_temp_files

THIS_DIR = pathlib.Path(__file__).absolute().parent
SAMPLES_DIR = THIS_DIR / "samples"


class TestGenerateImage:
    """Tests of generate_image function."""

    @pytest.mark.asyncio
    async def test_generate_simple_image(self) -> None:
        """Generate a simple image."""
        work_dir = SAMPLES_DIR / "simple"
        await remove_temp_files(work_dir)

        await generate_image(str(work_dir / "image.tex"))
        assert (work_dir / "image.pdf").exists()
        assert (work_dir / "image.png").exists()

    @pytest.mark.asyncio
    async def test_generate_japanese_image(self) -> None:
        """Generate an image with Japanese."""
        work_dir = SAMPLES_DIR / "japanese"
        await remove_temp_files(work_dir)

        await generate_image(str(work_dir / "image.tex"))
        assert (work_dir / "image.pdf").exists()
        assert (work_dir / "image.png").exists()

    @pytest.mark.asyncio
    async def test_generate_tex_file_image(self) -> None:
        """Try to generate an image from an invalid TeX file."""
        work_dir = SAMPLES_DIR / "invalid_tex_file"
        await remove_temp_files(work_dir)

        with pytest.raises(Exception):
            await generate_image(str(work_dir / "image.tex"))
