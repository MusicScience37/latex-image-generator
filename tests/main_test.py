"""Test of main.py."""

import asyncio
import pathlib

import click.testing
import imagehash
import PIL.Image

from latex_image_generator.main import main

from .remove_temp_files import remove_temp_files

THIS_DIR = pathlib.Path(__file__).absolute().parent
SAMPLES_DIR = THIS_DIR / "samples"
REFERENCE_DIR = THIS_DIR / "sample_reference"


def get_image_hash(path: pathlib.Path) -> imagehash.ImageHash:
    """Get a hash of an image.

    Args:
        path (pathlib.Path): Path of the image.

    Returns:
        imagehash.ImageHash: Hash of the image.
    """
    # Convert to non-transparent image. Otherwise, imagehash library won't work.
    origin = PIL.Image.open(str(path))
    non_transparent = PIL.Image.new("RGBA", origin.size, "WHITE")
    non_transparent.paste(origin, (0, 0), origin)

    return imagehash.phash_simple(non_transparent)


def assert_image(*, generated: pathlib.Path, reference: pathlib.Path) -> None:
    """Assert that images are similar.

    Args:
        generated (pathlib.Path): Path of the generated image.
        reference (pathlib.Path): Path of the reference image.
    """
    assert str(get_image_hash(generated)) == str(get_image_hash(reference))


class TestMain:
    """Test of main function."""

    def test_generate_simple_image(self) -> None:
        """Generate a simple image."""
        work_dir = SAMPLES_DIR / "simple"
        asyncio.run(remove_temp_files(work_dir))

        runner = click.testing.CliRunner()
        result = runner.invoke(main, [str(work_dir / "image.tex")])

        assert result.exit_code == 0
        assert_image(
            generated=work_dir / "image.png",
            reference=REFERENCE_DIR / "simple" / "image.png",
        )

    def test_generate_japanese_image(self) -> None:
        """Generate an image with Japanese."""
        work_dir = SAMPLES_DIR / "japanese"
        asyncio.run(remove_temp_files(work_dir))

        runner = click.testing.CliRunner()
        result = runner.invoke(main, [str(work_dir / "image.tex")])

        assert result.exit_code == 0
        assert_image(
            generated=work_dir / "image.png",
            reference=REFERENCE_DIR / "japanese" / "image.png",
        )

    def test_execute_without_argument(self) -> None:
        """Execute this tool without argument."""
        runner = click.testing.CliRunner()
        result = runner.invoke(main, [])
        assert result.exit_code != 0

    def test_execute_with_too_many_arguments(self) -> None:
        """Execute this tool with too many arguments."""
        work_dir = SAMPLES_DIR / "multi"
        asyncio.run(remove_temp_files(work_dir))

        runner = click.testing.CliRunner()
        result = runner.invoke(
            main, [str(work_dir / "image1.tex"), str(work_dir / "image2.tex")]
        )
        assert result.exit_code != 0

    def test_execute_with_directory(self) -> None:
        """Execute this tool with a directory."""
        work_dir = SAMPLES_DIR / "empty"
        asyncio.run(remove_temp_files(work_dir))

        runner = click.testing.CliRunner()
        result = runner.invoke(main, [str(work_dir)])
        assert result.exit_code != 0

    def test_execute_with_nonexisting_path(self) -> None:
        """Execute this tool with a non-existing path."""
        runner = click.testing.CliRunner()
        result = runner.invoke(main, [str(SAMPLES_DIR / "non_existing.tex")])
        assert result.exit_code != 0
