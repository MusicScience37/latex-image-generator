"""Remove temporary files."""

import pathlib
import typing

import aiofiles.os


async def remove_temp_files(dir_path: pathlib.Path) -> None:
    """Remove temporary files.

    Args:
        dir_path (pathlib.Path): Directory path.
    """
    filepath_str_list: typing.List[str] = await aiofiles.os.listdir(  # type: ignore
        str(dir_path)
    )  # type: ignore
    for filepath_str in filepath_str_list:
        if not filepath_str.endswith(".tex"):
            await aiofiles.os.remove(dir_path / filepath_str)
