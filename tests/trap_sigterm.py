"""Trap SIGTERM."""

import signal
import time


def _signal_handler(*_):
    # Do nothing.
    pass


def main() -> None:
    """Main function."""
    signal.signal(signal.SIGTERM, _signal_handler)

    time.sleep(10.0)


if __name__ == "__main__":
    main()
